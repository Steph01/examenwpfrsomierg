﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenWPF.Interfaces
{
    public interface IToFile
    {
        void ToXml(string filepath);
    }
}
