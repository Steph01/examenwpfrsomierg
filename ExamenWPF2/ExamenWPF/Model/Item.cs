﻿using ExamenWPF.Interfaces;
using ExamenWPF.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenWPF.Model
{
    [Serializable]
    public class Person : IToFile, IFromFile<>
    {
        public string item { get; set; }


        public void ToXml(string filepath)
        {
            XmlSerialization.WriteToXmlFile(filepath, this);
        }


    }
}
